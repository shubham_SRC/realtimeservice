
/**
 * Socket.io configuration
 */
'use strict';
var realTimeEventManager = require('../shareAPIs/RealTimeEventManager');

export default function (socketio) {
    //socketio.set('transports', ['websocket']);  
    realTimeEventManager.initiliase(socketio);
    socketio.on('connection', function (socket) {
        socket.address = `${socket.request.connection.remoteAddress}:${socket.request.connection.remotePort}`;

        socket.connectedAt = new Date();

        socket.log = function (...data) {
            console.log(`SocketIO ${socket.nsp.name} [${socket.address}]`, ...data);
        };

        realTimeEventManager.addSocket(socket, function(){
            console.log('******************** \n \n \n \n \n socket connected \n \n \n \n \n ********************');
        });

        // Call onDisconnect.
        socket.on('disconnect', () => {
            //onDisconnect(socket);
            // console.log('socketsocketsocketsocket  ++++++++++++++++++++++++ + ' + socket.id);
            socket.log('DISCONNECTED');
            realTimeEventManager.removeSocket(socket, function(){
                console.log('******************** \n \n \n \n \n socket disconnected \n \n \n \n \n ********************'); 
            });
        });

        // Call onConnect.
        //onConnect(socket, socketio);
        socket.log('CONNECTED');
    });
}

