'use strict';

 var appConfig = require('./config/appConfig.json')
,   prodAppConfigPath = '/etc/appConfig.json'
,   _ = require('lodash')
,   prodAppConfig = {} 
;

try{
  prodAppConfig = require(prodAppConfigPath);
}catch(e){
  prodAppConfig = {}; 
  console.log('production config not found');
}
_.extend(appConfig, prodAppConfig);

console.log(appConfig);

global.appConfig = appConfig;

// Set default node environment to development
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if(env === 'development' || env === 'test') {
  // Register the Babel require hook
  require('babel-register');
}

// Export the application
exports = module.exports = require('./app');
