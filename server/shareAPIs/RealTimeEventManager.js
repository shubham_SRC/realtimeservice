module.exports = new RealTimeEventManager();

function RealTimeEventManager(){
    
    console.log('**************** inside RealTimeEventManager ***************');

	var self = this
	,   socketMap = {}
	,   io = null
	,   SystemWidePubSub = require('./SystemWidePubSub')
	;

	function initiliase(socketio){
        io = socketio;
		SystemWidePubSub.initiliase(self);
	}

	function addSocket(socket, cb){
		//console.log(socket.handshake);
        var socketId = getSocketId(socket)
        ,   map = {}
        ;
        map[socketId] = socketId;
        socketMap[socketId] = socket;
        addSubscriptionChannel(socket, map);
        cb();
	}

	function removeSocket(socket, cb){
		//console.log(socket.handshake);
		var socketId = getSocketId(socket)
		,   map = {}
		;
		map[socketId] = socketId;
		removeSubscriptionChannel(socket, map);
		if(socket){
			socket.removeAllListeners();
			socket.disconnect();
		    delete socketMap[socketId];
		}
		cb();
	}

	function publishToAll(event,data){
        io.emit(event,data);
    }

    function publishToChannel(channelName, event, data){
        //console.log('inside publishToChannel',channelName,event,data);
        io.sockets.in(channelName).emit(event,data);
    }

    function addSubscriptionChannel(socket, map){
    	console.log('addSubscriptionChannel map ',map);
        if(map){
            for(var key in map){
                socket.join(map[key]);
            }
        }
    }

    function removeSubscriptionChannel(socket, map){
    	console.log('removeSubscriptionChannel map ',map);
        if(map){
            for(var key in map){
                socket.leave(map[key]);
            }
        }
    }

	function getSocketId(socket){
        var handshake = socket.handshake;
        if(handshake['query'] && handshake['query']['userId']){
        	return handshake['query']['userId'];
        }
        return socket.id;
	}

	this.addSocket = addSocket;
	this.initiliase = initiliase;
	this.removeSocket = removeSocket;
	this.publishToAll = publishToAll;
	this.publishToChannel = publishToChannel;
}