module.exports = new SystemWidePubSub();

function SystemWidePubSub(){
    
    console.log('**************** inside SystemWidePubSub ***************');

	var self = this
    ,   socketInstance = null
    ,   config = null
	,   NRP    = require('node-redis-pubsub')
	,   nodePubSub = null
    ,   appConfig = global.appConfig
	;

	function initiliase(socketIns){
        config = appConfig.pubsub_config || null;
        if(!config || (config && !config.enable)){
            console.log("SystemWidePubSub not configured so socket may not work please check config for enabling.");
            return
        }
        //console.log(socketIns);
        socketInstance = socketIns;
        configureNRP();
	}

    function configureNRP(nrpConfig){
        var nrpConfig = {
            scope:'app',
            host:config.ip,
            port:config.port
        }
        if(appConfig.pubsub_config && appConfig.pubsub_config.password){
            nrpConfig['auth'] = appConfig.pubsub_config.password;
        }
        console.log('nrpConfig', nrpConfig);
        nodePubSub = new NRP(nrpConfig);
        nodePubSub.on("error", function(error){
            // Handle errors here
            console.log('error inside nodePubSub', error);
        });
        on('PUBLISH_SOCKET_EVENT',handleMultipleServerLevelEvent);
    }

    function handleMultipleServerLevelEvent(data){
        console.log('inside handleMultipleServerLevelEvent', data);
        if(socketInstance && socketInstance[data.key]){
          socketInstance[data.key].apply(socketInstance, data.args);
          return;
        }
        console.log(`Handle Multiple Server Level Event failed because request is not valid request for this event : ${data.key}`);
    }

    function on(event,cb){
        if(!nodePubSub){
            console.log('SystemWidePubSub not configured so ON event failed');
        }
        nodePubSub.on(event,cb);
    }
 
    function emit(event,cb){
        if(!nodePubSub){
            console.log('SystemWidePubSub not configured so EMIT event failed');
        }
        nodePubSub.emit(event,cb);
    }
 
    function off(event,cb){
        if(!nodePubSub){
            console.log('SystemWidePubSub not configured so OFF event failed');
        }
        nodePubSub.off(event,cb);
    }

	this.initiliase = initiliase;
    this.on = on;
    this.off = off;
    this.emit = emit;
}