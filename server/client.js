const { io } = require("socket.io-client");

const socket = io("https://realtime.insuredmine.com", { 
    transports: ["websocket"],
    path: "/socket.io/",
    query : {
        userId : "shubham"
    } 
});

// client-side
socket.on("connect", () => {
    console.log(socket.id); // x8WIv7-mJelg7on_ALbx
  });
  socket.on("testing", (data) => {
    console.log(data); // x8WIv7-mJelg7on_ALbx
  });
  
  socket.on("disconnect", () => {
    console.log(socket.id); // undefined
  });

  socket.connect();