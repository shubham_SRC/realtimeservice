require("babel-core/register");
require("babel-polyfill");

'use strict';

import express from 'express';
import http from 'http';
var config = global.appConfig;
var cors = require('cors');

//Modify headers to parse request correctly
const overrideContentType = () => {
  return function(req, res, next) {
    if (req.headers['x-amz-sns-message-type']) {
        req.headers['content-type'] = 'application/json;charset=UTF-8';
    }
    next();
  };
}

process.setMaxListeners(0);
// Setup server
var app = express();
//app.use(bodyParser());
app.use(cors())
app.use(overrideContentType())

var server = http.createServer(app);
var socketio = require('socket.io')(server, {
  transports : ["websocket"]
});

require('./config/socketio').default(socketio);

// Start server
function startServer() {
  app.angularFullstack = server.listen(config.port, config.ip, function() {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
  });
}

setImmediate(startServer);
exports = module.exports = app;